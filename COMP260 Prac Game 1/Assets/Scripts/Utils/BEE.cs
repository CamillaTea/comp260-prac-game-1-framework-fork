﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BEE : MonoBehaviour {
    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;

    public float speed = 4.0f; // metres per second
    public float turnSpeed = 180.0f; // degrees per second
    public Transform target1;
    public Transform target2;
    private Vector2 heading = Vector2.right;


    public ParticleSystem explosionPrefab;


  
    // Use this for initialization
    void Start () {
        PlayerMove player1 = (PlayerMove)FindObjectOfType(typeof(PlayerMove));
        target1 = player1.transform;

        PlayerMove player2 = (PlayerMove)FindObjectOfType(typeof(PlayerMove));
        target2 = player2.transform;
    }
	
	// Update is called once per frame
	void Update () {
        // get the vector from the bee to the target
        Vector2 direction1 = target1.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;
        Vector2 direction;
        if (direction1.magnitude < direction2.magnitude)
        {
            direction = direction1;
        }
        else
        {
            direction = direction2;
        }

        
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);
    }
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);
    }


}

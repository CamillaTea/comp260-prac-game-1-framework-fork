﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour
{
    public int nBees = 50;
    public BEE beePrefab;
    public Rect spawnRect;    private int beeTracker = 0;    private float timer = 0.0f;    private float beePeriod = 5.0f;    private float minBeePeriod = 5.0f;    private float maxBeePeriod = 2.0f;    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }
    // Use this for initialization
    void Start()
    {
        timer = 0.0f;

    }



        // Update is called once per frame
        void Update()
        {
            timer += Time.deltaTime;

            if (timer > beePeriod)
            {
                if (beeTracker < nBees)
                {
                    // instantiate a bee
                    BEE bee = Instantiate(beePrefab);
                    // attach to this object in the hierarchy
                    bee.transform.parent = transform;
                    // give the bee a name and number
                    bee.gameObject.name = "Bee " + beeTracker;

                    // move the bee to a random position within
                    // the spawn rectangle
                    float x = spawnRect.xMin +
                    Random.value * spawnRect.width;
                    float y = spawnRect.yMin +
                    Random.value * spawnRect.height;
                    bee.transform.position = new Vector2(x, y);

                beeTracker++;
                beePeriod = Random.Range(minBeePeriod, maxBeePeriod);
                }
            timer = timer-beePeriod;
            }
        }
    

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }
}


